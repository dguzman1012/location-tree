# Installation guide #

## dependencies ##
- **Apache**: 2.4.x
    - **rewrite_module**
- 
- **PHP**>= 7.1.0
    - **curl**
    - **gd**
    - **mcrypt**
	- **json**
	- **XML**
- **Composer**>= 1.6.3

## installation ##

- Configure Apache virtual host for directory PATH_TO_PROJECT_IN_SERVER/public
- Copy project file to PATH_TO_PROJECT_IN_SERVER

```bash
cd PATH_TO_PROJECT_IN_SERVER
chmod -R 777 storage
chmod -R 777 bootstrap/cache
composer install
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan passport:install
```

- Configure DB Access into .env file (MYSQL, PostgreSQL, MSSQL, etc).

## technology stack ##
- **Laravel** 5.6



# How To:


This repository is a pre-configured project structure taken from Laravel. This Project is about an API to provide the control of your locations tree. There are some differences between world locations, and this API helps anyone to get locations with distinct territorial distributions.

![Locations](https://user-images.githubusercontent.com/5767551/36178198-e23fbf82-10f6-11e8-80f0-a38d4e1835ac.png)

## Register/Login

You need to get logged in order to get an access token. This token has to be passed via Header Authorization to get the locations.

![Register](https://user-images.githubusercontent.com/5767551/36178243-fe3f99e6-10f6-11e8-8604-16e2d054755a.png)

![Login](https://user-images.githubusercontent.com/5767551/36178277-13f52fa8-10f7-11e8-96bc-faf4ee6be4c3.png)

## Get Locations

You can get locations with the access token via Header Authorization. In the image below we are getting a location by name.

![LocationsByName](https://user-images.githubusercontent.com/5767551/36178341-481e4ddc-10f7-11e8-9cdb-39fe6cdf5bba.png)
