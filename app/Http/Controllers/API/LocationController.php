<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use Validator;
use Illuminate\Validation\Rule;

class LocationController extends Controller
{
    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::orderBy('id')
                ->limit(10)
                ->get();
        foreach ($locations as $location) {
            $location->parent = Location::getAncester($location->parent);   
        }
        return response()->json(['data' => $locations], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Location::VALIDATION);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $location = Location::create($input);
        $success['name'] =  $location->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $location = Location::find($id);
        if (is_null($location)){
            return response()->json(['error'=>array("message" => "Unable to find the Location")], 404);            
        }
        $location->parent = Location::getAncester($location->parent);
        return response()->json(['data' => $location->toArray()], $this->successStatus);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByName($name)
    {
        $locations = Location::
                where(function($query) use($name){
                    $query->whereRaw("LOWER(name) like LOWER(?)",  ['%'. $name. '%']);
                })
                ->limit(10)
                ->get();
        foreach ($locations as $location) {
            $location->parent = Location::getAncester($location->parent);   
        }
        return response()->json(['data' => $locations], $this->successStatus);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::find($id);
        if (is_null($location)){
            return response()->json(['error'=>array("message" => "Unable to find the Location")], 404);            
        }

        $validation = array(
            'entity_level' => 'in:country,region,county,municipality,city',
            'parent_id' => array(
                Rule::exists('locations')->where(function ($query) use($location) {
                    $query->where('id', $location->id);
                })
            )
        );

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $location->update($input);
        $success['name'] =  $location->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id);
        if (is_null($location)){
            return response()->json(['error'=>array("message" => "Unable to find the Location")], 404);            
        }

        Location::destroy($id);

        return response()->json(['success'=>array("message" => "Location deleted")], $this->successStatus);
    }
}
