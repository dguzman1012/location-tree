<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    const COUNTRY_LOCATION = 'country';
    const REGION_LOCATION = 'region';
    const COUNTY_LOCATION = 'county';
    const MUNICIPALITY_LOCATION = 'municipality';
    const CITY_LOCATION = 'city';

    const ORDER_LOCATION = [
    	self::COUNTRY_LOCATION 		=> 1,
    	self::REGION_LOCATION 		=> 2,
    	self::COUNTY_LOCATION 		=> 3,
    	self::MUNICIPALITY_LOCATION	=> 4,
    	self::CITY_LOCATION 		=> 5,
    ];

    const VALIDATION = [
    	'name' => 'required',
    	'entity_level' => 'required|in:country,region,county,municipality,city',
	    'parent_id' => 'exists:locations,id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'entity_level', 'parent_id',
    ];


    /**
     * Get the parent location.
     */
    public function parent()
    {
        return $this->hasOne('App\Location', 'id', 'parent_id');
    }

    public static function getAncester($element){
    	if (is_null($element)){
    		return null;
    	}else{
    		return self::getAncester($element->parent);
    	}
    	
    }

}
